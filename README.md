Modern CRM based on NestJS framework backend, MongoDB database, Angular 8 frontend.



## Description

Continuation of the HueCRM seed/starter application from https://github.com/hueSoft/hueCRM

## Stay in touch

- Author - [Piotr Szymanowski]
- Website - [https://huecrm.com - still in making](https://huecrm.com/)

## License

  hueCRM is [BSD 2](LICENSE) licensed.
